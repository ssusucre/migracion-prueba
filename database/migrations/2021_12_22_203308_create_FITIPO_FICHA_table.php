<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFITIPOFICHATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FITIPO_FICHA', function (Blueprint $table) {
            $table->integer('COD_TIPOFICHA', true);
            $table->string('TIPO_FICHA', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FITIPO_FICHA');
    }
}
