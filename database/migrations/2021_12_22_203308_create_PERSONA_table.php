<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePERSONATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PERSONA', function (Blueprint $table) {
            $table->integer('COD_PERSONA', true);
            $table->integer('COD_SALUD_SSU')->nullable();
            $table->string('NOMBRE', 50);
            $table->string('APELLIDO_PATERNO', 50)->nullable();
            $table->string('APELLIDO_MATERNO', 50)->nullable();
            $table->string('APELLIDO_ESPOSO', 50)->nullable();
            $table->string('EMAIL', 50)->nullable();
            $table->string('DIRECCION_DOMICILIO', 100)->nullable();
            $table->string('DIRECCION_TRABAJO', 100)->nullable();
            $table->string('TELEFONO_DOMICILIO', 50)->nullable();
            $table->string('TELEFONO_CELULAR', 50)->nullable();
            $table->string('TELEFONO_TRABAJO', 50)->nullable();
            $table->integer('COD_TIPO_IDENTIFICACION')->nullable();
            $table->string('NUMERO_IDENTIFICACION', 50)->nullable();
            $table->string('COMPLEMENTO', 20)->nullable();
            $table->integer('COD_ESTADO_PER')->nullable();
            $table->integer('COD_IDENT_PROCEDENCIA')->nullable();
            $table->integer('COD_GENERO');
            $table->integer('USR')->nullable();
            $table->dateTime('FHR')->nullable();
            $table->integer('COD_ENTIDADP')->nullable();
            $table->boolean('REGISTRO_SEGIP')->default(false);
            $table->dateTime('FECHA_SEGIP')->nullable();
            $table->integer('COD_ESTADOASUSS')->default(10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PERSONA');
    }
}
