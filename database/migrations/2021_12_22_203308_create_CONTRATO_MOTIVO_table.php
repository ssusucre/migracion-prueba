<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCONTRATOMOTIVOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CONTRATO_MOTIVO', function (Blueprint $table) {
            $table->integer('COD_MOTIVOCONTRATO', true);
            $table->string('MOTIVO', 50);
            $table->integer('COD_ENTIDADMC');
            $table->integer('COD_ESTADOMC');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CONTRATO_MOTIVO');
    }
}
