<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateESTADOOCURRENCIATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ESTADO_OCURRENCIA', function (Blueprint $table) {
            $table->integer('COD_ESTADO', true);
            $table->string('DESCRIPCION', 50);
            $table->integer('COD_CATEGORIA')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ESTADO_OCURRENCIA');
    }
}
