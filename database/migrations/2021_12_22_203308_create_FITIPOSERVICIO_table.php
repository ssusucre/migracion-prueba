<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFITIPOSERVICIOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FITIPOSERVICIO', function (Blueprint $table) {
            $table->integer('COD_TIPOSERVICIO');
            $table->integer('COD_ENTIDAD')->nullable();
            $table->string('TIPO_SERVICIO', 50)->nullable();
            $table->integer('ESTADO')->nullable();
            $table->integer('USUARIO')->nullable();
            $table->dateTime('FECHA_REGISTRO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FITIPOSERVICIO');
    }
}
