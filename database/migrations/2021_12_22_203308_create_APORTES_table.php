<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAPORTESTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('APORTES', function (Blueprint $table) {
            $table->integer('COD_APORTE', true);
            $table->integer('COD_PER_AFIL_ENTIDAD');
            $table->integer('COD_CONTRATO');
            $table->integer('COD_MES');
            $table->integer('YEAR_');
            $table->boolean('APORTE')->nullable();
            $table->integer('USR')->nullable();
            $table->dateTime('FHR')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('APORTES');
    }
}
