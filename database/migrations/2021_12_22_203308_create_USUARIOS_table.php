<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUSUARIOSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('USUARIOS', function (Blueprint $table) {
            $table->integer('COD_USUARIO', true);
            $table->string('NOMBRES', 100);
            $table->string('CI', 10);
            $table->string('USUARIO', 50);
            $table->string('PASSWORD', 15);
            $table->integer('COD_PERFIL_USUARIO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('USUARIOS');
    }
}
