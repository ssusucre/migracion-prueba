<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIDENTIFICACIONTIPOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('IDENTIFICACION_TIPO', function (Blueprint $table) {
            $table->integer('COD_TIPO_IDENTIFICACION', true);
            $table->integer('COD_TIPOIDENT_SSU');
            $table->string('TIPO_IDENTIFICACION', 50);
            $table->integer('COD_ESTADOTI');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('IDENTIFICACION_TIPO');
    }
}
