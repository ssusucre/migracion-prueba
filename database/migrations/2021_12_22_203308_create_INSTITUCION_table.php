<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateINSTITUCIONTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('INSTITUCION', function (Blueprint $table) {
            $table->integer('COD_INSTITUCION', true);
            $table->integer('COD_INSTSALUD_SSU')->nullable();
            $table->integer('COD_TIPO_INSTITUCION')->nullable();
            $table->string('NOMBRE', 80)->nullable();
            $table->string('DIRECCION', 50)->nullable();
            $table->string('TELEFONO', 50)->nullable();
            $table->string('NIT', 50)->nullable();
            $table->dateTime('FECHA_INICIO')->nullable();
            $table->integer('COD_REGIONAL')->nullable();
            $table->string('CODIGO_PATRONAL', 20)->nullable();
            $table->string('MATRICULA_COMERCIO', 20)->nullable();
            $table->string('REPRESENTANTE_LEGAL', 50)->nullable();
            $table->integer('COD_ESTADO_INST')->nullable();
            $table->integer('COD_ENTIDAD')->nullable();
            $table->integer('USR')->nullable();
            $table->dateTime('FHR')->nullable();
            $table->integer('COD_ESTADOI')->nullable()->default(7);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('INSTITUCION');
    }
}
