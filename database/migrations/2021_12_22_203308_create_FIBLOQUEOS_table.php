<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIBLOQUEOSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIBLOQUEOS', function (Blueprint $table) {
            $table->integer('COD_BLOQUEO', true);
            $table->integer('ENTIDAD')->nullable();
            $table->integer('COD_MEDICO')->nullable();
            $table->integer('COD_ESPECIALIDAD')->nullable();
            $table->date('FECHA_INICIO')->nullable();
            $table->date('FECHA_FINAL')->nullable();
            $table->string('OBSERVACIONES', 300)->nullable();
            $table->integer('USUARIO')->nullable();
            $table->dateTime('FECHA_REGISTRO')->nullable();
            $table->integer('COD_ESTADO')->nullable();
            $table->string('MOTIVO_DESBLOQUEO', 300)->nullable();
            $table->string('FICHAS_DESBLOQUEADAS', 100)->nullable();
            $table->integer('USUARIO_MOD')->nullable();
            $table->dateTime('FECHA_MODIFICADO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIBLOQUEOS');
    }
}
