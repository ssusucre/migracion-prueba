<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIFICHATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIFICHA', function (Blueprint $table) {
            $table->integer('COD_FICHA', true);
            $table->integer('COD_ENTIDAD')->nullable();
            $table->integer('CODPER_TITULAR')->nullable();
            $table->integer('CODPER_PACIENTE')->nullable();
            $table->integer('COD_MEDICO')->nullable();
            $table->integer('COD_ESPECIALIDAD')->nullable();
            $table->integer('COD_CONSULTORIO')->nullable();
            $table->integer('COD_TIPOFICHA')->nullable();
            $table->integer('COD_CLASEFICHA')->nullable();
            $table->integer('TURNO')->nullable();
            $table->integer('DIA')->nullable();
            $table->date('FECHA_FICHA')->nullable();
            $table->time('HORA_PRESENTACION', 7)->nullable();
            $table->integer('NUM_FICHA')->nullable();
            $table->integer('LUGAR')->nullable();
            $table->integer('ESTADO')->nullable();
            $table->string('OBSERVACIONES', 500)->nullable();
            $table->integer('COD_USUARIO')->nullable();
            $table->dateTime('FECHA_REGISTRO')->nullable();
            $table->integer('COD_CONTRATO')->nullable();
            $table->char('JUSTIFICACION', 50)->nullable();
            $table->integer('COD_BLOQUEO')->nullable();
            $table->string('OBS_TRANSFERENCIA', 300)->nullable();
            $table->string('OBS_DESBLOQUEO', 300)->nullable();
            $table->integer('DESBL_USUARIO')->nullable();
            $table->dateTime('DESBL_FECHA')->nullable();
            $table->integer('COD_CONEX')->nullable();
            $table->integer('COD_MEDESP')->nullable();
            $table->integer('COD_USUMOD')->nullable();
            $table->dateTime('FECHA_MODIFICADO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIFICHA');
    }
}
