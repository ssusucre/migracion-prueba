<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGRUPOSANGUINEOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GRUPO_SANGUINEO', function (Blueprint $table) {
            $table->integer('COD_GRUPOSANGUINEO', true);
            $table->integer('COD_GSSALUD_SSU')->nullable();
            $table->string('GRUPO_SANGUINEO', 10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GRUPO_SANGUINEO');
    }
}
