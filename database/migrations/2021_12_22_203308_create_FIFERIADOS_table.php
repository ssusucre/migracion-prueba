<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIFERIADOSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIFERIADOS', function (Blueprint $table) {
            $table->date('FECHA');
            $table->string('DESCRIPCION', 300);
            $table->integer('USUARIO');
            $table->dateTime('FECHA_REGISTRO');
            $table->integer('ESTADO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIFERIADOS');
    }
}
