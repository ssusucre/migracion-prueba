<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePERSONAAFILIADOENTIDADTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PERSONA_AFILIADO_ENTIDAD', function (Blueprint $table) {
            $table->integer('COD_PER_AFIL_ENTIDAD', true);
            $table->integer('COD_PERSONA');
            $table->integer('COD_AFILIADO');
            $table->integer('COD_ENTIDAD');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PERSONA_AFILIADO_ENTIDAD');
    }
}
