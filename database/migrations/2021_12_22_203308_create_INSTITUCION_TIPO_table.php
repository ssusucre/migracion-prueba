<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateINSTITUCIONTIPOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('INSTITUCION_TIPO', function (Blueprint $table) {
            $table->integer('COD_TIPO_INSTITUCION', true);
            $table->integer('COD_TIPOINST_SALUD_SSU')->nullable();
            $table->string('TIPO_INSTITUCION', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('INSTITUCION_TIPO');
    }
}
