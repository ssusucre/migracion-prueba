<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFICONSULTORIOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FICONSULTORIO', function (Blueprint $table) {
            $table->integer('COD_CONSULTORIO', true);
            $table->integer('TIPO_LUGAR')->nullable();
            $table->integer('COD_ENTIDAD')->nullable();
            $table->string('DESCRIPCION', 100)->nullable();
            $table->string('DIRECCION', 100)->nullable();
            $table->integer('TELEFONO')->nullable();
            $table->boolean('ESTADO')->nullable();
            $table->integer('COD_USUARIO')->nullable();
            $table->dateTime('FECHA')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FICONSULTORIO');
    }
}
