<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAFILIADOBITACORATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AFILIADO_BITACORA', function (Blueprint $table) {
            $table->integer('COD_BITACORAAF', true);
            $table->integer('COD_AFILIADO');
            $table->integer('COD_CONTRATO')->nullable();
            $table->integer('COD_TIPOBITACORA');
            $table->dateTime('FECHA_BITACORA');
            $table->string('MOTIVO_BITACORA', 30);
            $table->string('OBSERVACIONES', 30)->nullable();
            $table->integer('USR')->nullable();
            $table->dateTime('FHR')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AFILIADO_BITACORA');
    }
}
