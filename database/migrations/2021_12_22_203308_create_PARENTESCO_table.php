<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePARENTESCOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PARENTESCO', function (Blueprint $table) {
            $table->integer('COD_PARENTESCO', true);
            $table->integer('COD_PSALUD_SSU');
            $table->string('PARENTESCO', 20);
            $table->integer('COD_ESTADOP')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PARENTESCO');
    }
}
