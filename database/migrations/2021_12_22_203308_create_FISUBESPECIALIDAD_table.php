<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFISUBESPECIALIDADTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FISUBESPECIALIDAD', function (Blueprint $table) {
            $table->integer('COD_SUBESPECIALIDAD', true);
            $table->integer('COD_ESP_SALUD_SSU')->nullable();
            $table->integer('COD_ESPECIALIDAD');
            $table->integer('COD_ENTIDAD');
            $table->string('ESPECIALIDAD', 100)->nullable();
            $table->boolean('ESTADO')->nullable();
            $table->boolean('WEB')->nullable();
            $table->boolean('BIOMETRICO')->nullable();
            $table->string('USUARIO', 50)->nullable();
            $table->dateTime('FECHA')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FISUBESPECIALIDAD');
    }
}
