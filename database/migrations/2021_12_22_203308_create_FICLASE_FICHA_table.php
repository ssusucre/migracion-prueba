<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFICLASEFICHATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FICLASE_FICHA', function (Blueprint $table) {
            $table->integer('COD_CLASEFICHA', true);
            $table->string('CLASE_FICHA', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FICLASE_FICHA');
    }
}
