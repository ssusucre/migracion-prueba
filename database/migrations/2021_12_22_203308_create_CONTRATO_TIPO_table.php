<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCONTRATOTIPOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CONTRATO_TIPO', function (Blueprint $table) {
            $table->integer('COD_TIPOCONTRATO', true);
            $table->integer('COD_TCSALUD_SSU');
            $table->string('DESCRIPCION', 50);
            $table->integer('COD_ESTADOTC');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CONTRATO_TIPO');
    }
}
