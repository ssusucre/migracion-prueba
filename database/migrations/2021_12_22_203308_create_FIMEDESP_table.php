<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIMEDESPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIMEDESP', function (Blueprint $table) {
            $table->integer('COD_MEDESP', true);
            $table->integer('COD_MEDICO')->nullable();
            $table->integer('COD_SUBESPECIALIDAD')->nullable();
            $table->integer('COD_TIPOMEDICO')->nullable();
            $table->date('FECHA_INICIO')->nullable();
            $table->date('FECHA_FIN')->nullable();
            $table->string('OBSERVACION', 300)->nullable();
            $table->integer('COD_ENTIDAD')->nullable();
            $table->integer('ESTADO')->nullable();
            $table->integer('COD_USUARIO')->nullable();
            $table->dateTime('FECHA_REGISTRO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIMEDESP');
    }
}
