<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCERTIFICADONOAFILIACIONTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CERTIFICADO_NOAFILIACION', function (Blueprint $table) {
            $table->integer('CODIGO', true);
            $table->string('CARNET_IDENTIDAD', 50);
            $table->string('NOMBRES_APELLIDOS', 100);
            $table->dateTime('FECHA_NACIMIENTO');
            $table->dateTime('FECHA_EMISION')->nullable();
            $table->string('LUGAR_EMISION', 50)->nullable();
            $table->string('CODIGO_CERTIFICADO', 30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CERTIFICADO_NOAFILIACION');
    }
}
