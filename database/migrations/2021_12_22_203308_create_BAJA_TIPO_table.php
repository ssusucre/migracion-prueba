<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBAJATIPOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BAJA_TIPO', function (Blueprint $table) {
            $table->integer('COD_TIPOBAJA', true);
            $table->integer('COD_TBSALUD_SSU');
            $table->string('TIPO_BAJA', 70);
            $table->integer('COD_ENTIDAD')->nullable();
            $table->integer('COD_ESTADOTB')->nullable()->default(7);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('BAJA_TIPO');
    }
}
