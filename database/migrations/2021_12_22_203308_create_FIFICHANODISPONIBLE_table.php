<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIFICHANODISPONIBLETable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIFICHANODISPONIBLE', function (Blueprint $table) {
            $table->integer('COD_NODISPONIBLE', true);
            $table->integer('COD_ENTIDAD')->nullable();
            $table->integer('COD_SUBESPECIALIDAD')->nullable();
            $table->string('OBSERVACIONES', 300)->nullable();
            $table->date('FECHA_SOLICITUD')->nullable();
            $table->integer('ESTADO')->nullable();
            $table->integer('USUARIO')->nullable();
            $table->dateTime('FECHA_REGISTRO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIFICHANODISPONIBLE');
    }
}
