<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIPARAMETROSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIPARAMETROS', function (Blueprint $table) {
            $table->integer('COD_PARAMETRO')->primary();
            $table->string('NOMBRE_PARAMETRO', 50)->nullable();
            $table->string('VALOR_PARAMETRO', 50)->nullable();
            $table->string('DESCRIPCION_PARAMETRO', 300)->nullable();
            $table->integer('ENTIDAD')->nullable();
            $table->integer('COD_USUARIO')->nullable();
            $table->dateTime('FECHA')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIPARAMETROS');
    }
}
