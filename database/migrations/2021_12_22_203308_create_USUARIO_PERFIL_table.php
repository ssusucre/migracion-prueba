<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUSUARIOPERFILTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('USUARIO_PERFIL', function (Blueprint $table) {
            $table->integer('COD_PERFIL_USUARIO', true);
            $table->string('DESCRIPCION', 50);
            $table->integer('COD_ENTIDAD');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('USUARIO_PERFIL');
    }
}
