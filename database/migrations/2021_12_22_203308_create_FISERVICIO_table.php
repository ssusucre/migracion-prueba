<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFISERVICIOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FISERVICIO', function (Blueprint $table) {
            $table->integer('COD_SERVICIO', true);
            $table->string('SERVICIO', 150)->nullable();
            $table->string('OBSERVACIONES', 200)->nullable();
            $table->integer('ESTADO')->nullable();
            $table->integer('USUARIO')->nullable();
            $table->dateTime('FECHA_REGISTRO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FISERVICIO');
    }
}
