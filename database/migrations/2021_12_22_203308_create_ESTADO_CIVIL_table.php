<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateESTADOCIVILTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ESTADO_CIVIL', function (Blueprint $table) {
            $table->integer('COD_ESTADOCIVIL', true);
            $table->integer('COD_ECSALUD_SSU')->nullable();
            $table->string('ESTADO_CIVIL', 30);
            $table->integer('COD_ESTADOEC')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ESTADO_CIVIL');
    }
}
