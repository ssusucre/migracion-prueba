<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCONTRATOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CONTRATO', function (Blueprint $table) {
            $table->integer('COD_CONTRATO', true);
            $table->integer('COD_CONTRATOSALUD_SSU')->nullable();
            $table->integer('COD_PER_AFIL_ENTIDAD');
            $table->integer('COD_AFILIADO')->nullable();
            $table->integer('COD_AFILIADOSALUD_SSU')->nullable();
            $table->string('CODIGO_REFERENCIA', 20)->nullable();
            $table->string('CODIGO_ALTERNATIVO', 20)->nullable();
            $table->integer('COD_TIPOAFILIADO')->nullable();
            $table->integer('COD_CLASEAFILIADO')->nullable();
            $table->integer('COD_TITULAR')->nullable();
            $table->integer('COD_PARENTESCO')->nullable();
            $table->integer('COD_INSTITUCION');
            $table->integer('COD_TIPOCONTRATO');
            $table->integer('COD_MOTIVOCONTRATO');
            $table->integer('COD_REPARTICION')->nullable();
            $table->date('FECHA_INICIO');
            $table->date('FECHA_FIN');
            $table->date('FECHA_CESANTIA');
            $table->string('MOTIVO_CONTRATO', 50)->nullable();
            $table->string('OBSERVACIONES_CONTRATO', 150)->nullable();
            $table->integer('COD_ESTADO_CON');
            $table->integer('COD_TIPOBAJA')->nullable()->default(42)->comment('42 por defecto que relaciona a conclusion de relacion laboral');
            $table->dateTime('BAJA_FECHA')->nullable();
            $table->string('MOTIVO_BAJA', 200)->nullable();
            $table->integer('USR_CONTRATO')->nullable();
            $table->dateTime('FHR_CONTRATO')->nullable();
            $table->integer('USR_BAJA')->nullable();
            $table->dateTime('FHR_BAJA')->nullable();
            $table->boolean('ULTIMO_CONTRATO')->nullable();
            $table->integer('COD_ENTIDADC')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CONTRATO');
    }
}
