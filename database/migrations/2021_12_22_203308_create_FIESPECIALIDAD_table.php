<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIESPECIALIDADTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIESPECIALIDAD', function (Blueprint $table) {
            $table->integer('COD_ESPECIALIDAD', true);
            $table->string('DESCRIPCION', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIESPECIALIDAD');
    }
}
