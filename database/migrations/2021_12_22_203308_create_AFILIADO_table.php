<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAFILIADOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AFILIADO', function (Blueprint $table) {
            $table->integer('COD_AFILIADO', true);
            $table->integer('COD_SALUD_SSU');
            $table->integer('COD_DEPARTAMENTO');
            $table->integer('COD_ESTADOCIVIL');
            $table->integer('COD_GRUPOSANGUINEO')->nullable();
            $table->string('CODIGO_REFERENCIA', 20)->nullable();
            $table->string('CODIGO_ALTERNATIVO', 20)->nullable();
            $table->integer('COD_PROFESION')->nullable();
            $table->integer('COD_PARENTESCO')->nullable();
            $table->integer('COD_TITULAR')->nullable();
            $table->integer('COD_TITULARSALUDSSU')->nullable();
            $table->integer('COD_ESTADO_AF');
            $table->integer('COD_TIPOAFILIADO')->nullable()->comment('ADM, DOC, FALLECIDO, JUBILADO....');
            $table->integer('COD_CLASEAFILIADO')->nullable();
            $table->integer('COD_PARENTESCO_REFERENCIA')->nullable();
            $table->integer('COD_RESPACARGO')->nullable();
            $table->integer('COD_ULTIMO_CONTRATO')->nullable();
            $table->dateTime('FECHA_NAC');
            $table->string('MATRICULA', 20);
            $table->string('PERSONA_REFERENCIA', 50)->nullable();
            $table->string('TELEFONO_REFERENCIA', 30)->nullable();
            $table->string('LUGAR_RESIDENCIA', 100)->nullable();
            $table->string('LUGAR_RADICATORIA', 100)->nullable();
            $table->dateTime('FECHA_AFILIACION')->nullable();
            $table->string('OBS_AFILIADO', 100)->nullable();
            $table->string('FOTO', 50)->nullable();
            $table->integer('COD_ENTIDAD');
            $table->integer('USR')->nullable();
            $table->dateTime('FHR')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AFILIADO');
    }
}
