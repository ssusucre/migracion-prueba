<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAFILIADOTIPOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AFILIADO_TIPO', function (Blueprint $table) {
            $table->integer('COD_TIPOAFILIADO', true);
            $table->string('TIPO_AFILIADO', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AFILIADO_TIPO');
    }
}
