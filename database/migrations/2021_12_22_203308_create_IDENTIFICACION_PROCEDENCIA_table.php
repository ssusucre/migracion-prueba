<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIDENTIFICACIONPROCEDENCIATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('IDENTIFICACION_PROCEDENCIA', function (Blueprint $table) {
            $table->integer('COD_PROCIDENTIFICACION', true);
            $table->integer('COD_PISALUD_SSU');
            $table->string('PROCEDENCIA_IDENTIFICACION', 5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('IDENTIFICACION_PROCEDENCIA');
    }
}
