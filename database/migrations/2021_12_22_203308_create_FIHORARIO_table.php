<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIHORARIOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIHORARIO', function (Blueprint $table) {
            $table->integer('COD_MEDESP');
            $table->integer('DIA');
            $table->integer('TURNO');
            $table->integer('COD_CONSULTORIO');
            $table->integer('COD_ENTIDAD');
            $table->time('HORA_INICIO', 7)->nullable();
            $table->time('HORA_FIN', 7)->nullable();
            $table->integer('CUPO')->nullable();
            $table->integer('DIA_ANT_INI')->nullable();
            $table->time('HORA_INI_FICHAS', 7)->nullable();
            $table->integer('DIA_ANT_FIN')->nullable();
            $table->time('HORA_FIN_FICHAS', 7)->nullable();
            $table->integer('COD_USUARIO')->nullable();
            $table->dateTime('FECHA')->nullable();

            $table->primary(['COD_MEDESP', 'DIA', 'TURNO']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIHORARIO');
    }
}
