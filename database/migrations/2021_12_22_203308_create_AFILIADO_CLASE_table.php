<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAFILIADOCLASETable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AFILIADO_CLASE', function (Blueprint $table) {
            $table->integer('COD_CLASEAFILIADO', true);
            $table->integer('COD_CASALUD_SSU');
            $table->string('DESCRIPCION', 40);
            $table->integer('COD_ENTIDAD')->nullable();
            $table->integer('COD_ESTADOCF')->nullable()->default(7)->comment('habilitado=7 deshabilitado=6');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AFILIADO_CLASE');
    }
}
