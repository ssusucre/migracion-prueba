<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFISERVICIOINSTITUCIONTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FISERVICIO-INSTITUCION', function (Blueprint $table) {
            $table->integer('COD_SERINST', true);
            $table->integer('COD_INSTITUCION');
            $table->integer('COD_TIPOSERVICIO');
            $table->string('OBSERVACIONES', 300)->nullable();
            $table->integer('COSTO')->nullable();
            $table->date('FECHA_INICIO')->nullable();
            $table->date('FECHA_FIN')->nullable();
            $table->integer('ESTADO')->nullable();
            $table->integer('USUARIO')->nullable();
            $table->dateTime('FECHA_REGISTRO')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FISERVICIO-INSTITUCION');
    }
}
