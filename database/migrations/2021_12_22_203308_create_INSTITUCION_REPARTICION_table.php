<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateINSTITUCIONREPARTICIONTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('INSTITUCION_REPARTICION', function (Blueprint $table) {
            $table->integer('COD_INSTITUCION');
            $table->integer('COD_REPARTICION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('INSTITUCION_REPARTICION');
    }
}
