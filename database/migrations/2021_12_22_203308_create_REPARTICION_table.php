<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateREPARTICIONTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('REPARTICION', function (Blueprint $table) {
            $table->integer('COD_REPARTICION', true);
            $table->string('REPARTICION', 50);
            $table->integer('COD_ENTIDAD')->nullable();
            $table->integer('COD_ESTADOR')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('REPARTICION');
    }
}
