<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAFILIADOTIPOBITACORATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AFILIADO_TIPOBITACORA', function (Blueprint $table) {
            $table->integer('COD_TIPOBITACORA', true);
            $table->string('DESCRIPCION', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AFILIADO_TIPOBITACORA');
    }
}
