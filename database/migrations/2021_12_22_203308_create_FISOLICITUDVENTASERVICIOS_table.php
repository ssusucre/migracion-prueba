<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFISOLICITUDVENTASERVICIOSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FISOLICITUDVENTASERVICIOS', function (Blueprint $table) {
            $table->integer('COD_SOLICITUD', true);
            $table->date('FECHA_SOLICITUD')->nullable();
            $table->integer('COD_ENTIDAD')->nullable();
            $table->integer('CODPER_TITULAR')->nullable();
            $table->integer('COD_PER_PACIENTE')->nullable();
            $table->integer('COD_CONTRATO')->nullable();
            $table->integer('COD_MEDSOLICITANTE')->nullable();
            $table->integer('COD_ESPECIALIDAD')->nullable();
            $table->integer('COD_TIPOSERVICIO')->nullable();
            $table->integer('COD_INST_SER')->nullable();
            $table->integer('COD_INSTITUCION')->nullable();
            $table->integer('COD_SERVICIO')->nullable();
            $table->integer('COD_LUGAR')->nullable();
            $table->string('OBSERVACIONES', 500)->nullable();
            $table->integer('COD_ESTADO')->nullable();
            $table->dateTime('FECHA_REGISTRO')->nullable();
            $table->integer('USUARIO')->nullable();
            $table->string('SOLICITANTE', 200)->nullable();
            $table->string('CARGO', 200)->nullable();
            $table->string('DIAGNOSTICO', 500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FISOLICITUDVENTASERVICIOS');
    }
}
