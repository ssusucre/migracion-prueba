<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHUELLASDACTILARESTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('HUELLAS_DACTILARES', function (Blueprint $table) {
            $table->integer('CodPersona')->nullable();
            $table->integer('CodEntidad')->nullable();
            $table->integer('Cod_Per_Afi')->nullable();
            $table->binary('Izq_1')->nullable();
            $table->binary('Izq_2')->nullable();
            $table->binary('Izq_3')->nullable();
            $table->binary('Izq_4')->nullable();
            $table->binary('Izq_5')->nullable();
            $table->binary('Der_1')->nullable();
            $table->binary('Der_2')->nullable();
            $table->binary('Der_3')->nullable();
            $table->binary('Der_4')->nullable();
            $table->binary('Der_5')->nullable();
            $table->dateTime('Fecha_Hora')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('HUELLAS_DACTILARES');
    }
}
