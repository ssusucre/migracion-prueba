<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFIMEDICOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FIMEDICO', function (Blueprint $table) {
            $table->integer('COD_MEDICO', true);
            $table->integer('COD_MED_SALUD_SSU')->nullable();
            $table->integer('COD_PERSONA')->nullable();
            $table->integer('TIPO_MEDICO')->nullable();
            $table->integer('COD_ENTIDAD')->nullable();
            $table->string('MATRICULA_MED', 30)->nullable();
            $table->integer('COD_ESPECIALIDAD')->nullable();
            $table->integer('COD_LUGAR')->nullable();
            $table->integer('ESTADO_MEDICO')->nullable();
            $table->string('USUARIO', 50)->nullable();
            $table->dateTime('FECHA')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FIMEDICO');
    }
}
