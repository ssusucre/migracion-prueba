<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFITIPOMEDICOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FITIPOMEDICO', function (Blueprint $table) {
            $table->integer('COD_TIPOMEDICO', true);
            $table->string('TIPO_MEDICO', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FITIPOMEDICO');
    }
}
